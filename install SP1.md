# Instalace
Pro spuštení je potřeba mít nainstalovaný Maven.

**Build project:**
```shell
cd ./sentiment-microservices
mvn package
```
**Run project:**
```shell
#cd ./sentiment-microservices

#Run-1 discovery-server
cd ./discovery-server
mvn spring-boot:run

#Run-2 crypto-app
cd ./crypto-app
mvn spring-boot:run

#Run-3 vaadin-ui
cd ./vaadin-ui
mvn spring-boot:run

#Run-4 twitter-app
##TODO !!!!!!
```
**Build & Run project:**
```shell
cd ./sentiment-microservices
mvn package spring-boot:repackage
#todo ...
```

## Configurace portů
Ve spring-projectu lze porty naconfigurovat v souboru .sentiment-microservices/{spring-app}/src/main/resources/bootstrap.yml

Defaultní konfigurace je:
* **discovery-server** : http://localhost:7001/
* **crypto-app**: http://localhost:9401/
* **vaadin-ui**: http://localhost:9601/
* **twitter-app**: http://localhost:????/

Případně pomocí přepínače:

```shell
#mvn package spring-boot:repackage
java -Dserver.port=9602 -jar #target-HERE
```

# Screenshots
 ![archytektura_aplikace](uploads/ui%20screen.png) ![archytektura_aplikace](uploads/ui%20screen%202.png)

# Architektura aplikace
![archytektura_aplikace](uploads/archytektura%20aplikace.png)
