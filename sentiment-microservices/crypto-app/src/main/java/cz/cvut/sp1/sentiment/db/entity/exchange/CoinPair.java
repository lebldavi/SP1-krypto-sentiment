package cz.cvut.sp1.sentiment.db.entity.exchange;

import cz.cvut.sp1.sentiment.db.entity.AbstractEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Data @EqualsAndHashCode(callSuper = false)
@Builder
@Entity
public class CoinPair extends AbstractEntity {

    @ManyToOne(targetEntity = Coin.class)
    @NotNull
    private Coin base;
    @ManyToOne(targetEntity = Coin.class)
    @NotNull
    private Coin counter;
}
