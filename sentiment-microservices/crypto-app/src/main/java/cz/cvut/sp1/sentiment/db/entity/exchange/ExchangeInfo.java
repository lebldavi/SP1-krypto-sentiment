package cz.cvut.sp1.sentiment.db.entity.exchange;

import cz.cvut.sp1.sentiment.db.entity.AbstractEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class ExchangeInfo extends AbstractEntity{

    @NotEmpty
    private String name;
    private boolean enabled = true;

    private String web;

    @ManyToMany(targetEntity = CoinPair.class)
    private Set<CoinPair> coinPairSet;

}
