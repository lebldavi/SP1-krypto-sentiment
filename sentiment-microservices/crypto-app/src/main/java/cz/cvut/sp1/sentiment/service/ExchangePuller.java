package cz.cvut.sp1.sentiment.service;

import org.knowm.xchange.Exchange;
import org.knowm.xchange.service.marketdata.MarketDataService;

public abstract class ExchangePuller {
    Exchange exchange;
    MarketDataService dataService;
}
