package cz.cvut.sp1.sentiment.db.entity.exchange;

import cz.cvut.sp1.sentiment.db.entity.AbstractEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class CoinSubscribe extends AbstractEntity{

    @ManyToOne(targetEntity = Coin.class)
    private Coin coin;

    private boolean subscribe = true;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDate;
}
