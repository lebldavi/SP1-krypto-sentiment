package cz.cvut.sp1.sentiment.controller;

import cz.cvut.sp1.sentiment.db.SentimentsRepository;
import cz.cvut.sp1.sentiment.db.entity.exchange.Coin;
import cz.cvut.sp1.sentiment.db.entity.sentiment.Sentiment;
import cz.cvut.sp1.sentiment.service.SentimentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


@RestController
@RequestMapping(value = "/sentiment-data", produces = MediaType.APPLICATION_JSON_VALUE)
public class SentimentController {
    private static final Logger log = LoggerFactory.getLogger(SentimentController.class);

    final SentimentService service;
    final SentimentsRepository repository;

    @Autowired
    public SentimentController(SentimentService service, SentimentsRepository repository) {
        this.service = service;
        this.repository = repository;
    }

    @PostMapping("/{coin}")
    @ResponseStatus(HttpStatus.OK)
    public void putResult(@PathVariable(name = "coin") String coin, @RequestBody Sentiment sentiment) {
        log.info("putResult - coin: {}, value: {}", coin, sentiment.getSentimentValue());
        service.addSentimentBy(coin, sentiment);
    }

    @GetMapping("/{coin}")
    public List<Sentiment> getByCoin(@PathVariable("coin") String coin) {
        log.info("getByCoin: {}", coin);
        return repository.findAllByCoin_CommonCode(coin);
    }

    @GetMapping()
    public List<Sentiment> getLatest() {
        log.info("getLatest");
        return repository.findAllByOrderByIdDesc();
    }
}
