package cz.cvut.sp1.sentiment.controller;

import cz.cvut.sp1.sentiment.db.CoinSubscribeRepository;
import cz.cvut.sp1.sentiment.db.entity.exchange.Coin;
import cz.cvut.sp1.sentiment.service.SubscribeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/coin-subscription", produces = MediaType.APPLICATION_JSON_VALUE)
public class SubscriptionController {

    private final SubscribeService service;

    @Autowired
    public SubscriptionController(SubscribeService subscribeService) {
        this.service = subscribeService;
    }

    @GetMapping
    public Coin getCoinToSubscribe() {
        return getPrimaryCoinToSubscribe();
    }

    @GetMapping("/hot")
    public Coin getPrimaryCoinToSubscribe() {
        return service.getSubscribedCoin();
    }

    @PostMapping("/{coin}")
    public Coin setHotCoinToSubscribe(@PathVariable("coin") String coin) {
        return service.setSingleSubscription(coin);
    }
}
