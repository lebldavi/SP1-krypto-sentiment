package cz.cvut.sp1.sentiment.service;

import lombok.extern.slf4j.Slf4j;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.binance.BinanceExchange;
import org.knowm.xchange.binance.dto.marketdata.BinancePrice;
import org.knowm.xchange.binance.dto.marketdata.BinanceTicker24h;
import org.knowm.xchange.binance.service.BinanceMarketDataService;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
@Slf4j
public class BinanceDataPuller extends ExchangePuller implements ExchangeData {

    private List<BinancePrice> prices = new ArrayList<>();

    private List<CurrencyPair> pullerList;

    private Map<CurrencyPair, Ticker> tickerMap = new HashMap<>();
    @Autowired
    public BinanceDataPuller() {
        exchange = ExchangeFactory.INSTANCE.createExchange(BinanceExchange.class);
        dataService = exchange.getMarketDataService();
    }
    public List<BinancePrice> getPrices()
    {
        return prices;
    }

    private BinanceExchange getExchange() {
        return (BinanceExchange) exchange;
    }

    private BinanceMarketDataService getDataService() {
        return (BinanceMarketDataService) dataService;
    }

    //pull prices every 2 minute
    @Scheduled(fixedRate = 120_000)
    private void pullAllPrices() {
        try {
            prices.clear();
            prices.addAll(getDataService().tickerAllPrices());
            log.info("Pulling prices: " + prices.size());
        } catch (IOException e) {
            e.printStackTrace();
            prices.clear();
        }
    }

}
