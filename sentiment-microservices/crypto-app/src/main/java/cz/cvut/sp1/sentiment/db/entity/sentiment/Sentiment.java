package cz.cvut.sp1.sentiment.db.entity.sentiment;

import cz.cvut.sp1.sentiment.db.entity.AbstractEntity;
import cz.cvut.sp1.sentiment.db.entity.exchange.Coin;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Data @EqualsAndHashCode(callSuper = false)
public class Sentiment extends AbstractEntity {

    private Double sentimentValue = 0.0;

    @Temporal(TemporalType.TIMESTAMP)
    private Date timeStamp;

    private String tweetMessage;

    @ManyToOne(targetEntity = Coin.class)
    @NotNull
    private Coin coin;

    public Sentiment() {
        //JPA
    }

    public Sentiment(Double sentimentValue, Date timeStamp, String tweetMessage, Coin coin) {
        this.sentimentValue = sentimentValue;
        this.timeStamp = timeStamp;
        this.tweetMessage = tweetMessage;
        this.coin = coin;
    }
}
