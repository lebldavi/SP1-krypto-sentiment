package cz.cvut.sp1.sentiment;

import com.google.common.collect.Lists;
import cz.cvut.sp1.sentiment.db.CoinsRepository;
import cz.cvut.sp1.sentiment.db.entity.exchange.Coin;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Arrays;
import java.util.List;

@EnableEurekaClient
@SpringBootApplication(scanBasePackages =
		{"cz.cvut.sp1.sentiment.service", "cz.cvut.sp1.sentiment.db", "cz.cvut.sp1.sentiment.controller"})
@EnableScheduling
public class CryptoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CryptoApplication.class, args);
	}

	@Bean
	CommandLineRunner initCoins(CoinsRepository coinsRepository) {
		return (args) -> {
            Coin btc = coinsRepository.findByCommonCode("BTC");
            if(btc != null)
                return;
            List<Coin> list = Arrays.asList(
                    new Coin("Bitcoin", "BTC", Arrays.asList("BTC", "Bitcoin")),
                    new Coin("Ethereum", "ETH", Arrays.asList("ETH", "Ethereum")),
                    new Coin("Litecoin", "LTC", Arrays.asList("LTC", "Litecoin")),
                    new Coin("Monero", "XMR", Arrays.asList("XMR", "Monero")),
                    new Coin("Cardano", "ADA", Arrays.asList("ADA", "Cardano")),
                    new Coin("Dogecoin", "DOGE", Arrays.asList("DOGE", "Dogecoin"))
            );
            coinsRepository.saveAll(list);
        };

	}

}
