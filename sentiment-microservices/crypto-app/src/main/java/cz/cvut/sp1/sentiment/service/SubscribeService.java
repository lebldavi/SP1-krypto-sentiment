package cz.cvut.sp1.sentiment.service;

import cz.cvut.sp1.sentiment.db.CoinSubscribeRepository;
import cz.cvut.sp1.sentiment.db.CoinsRepository;
import cz.cvut.sp1.sentiment.db.entity.exchange.Coin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Iterator;

@Service
public class SubscribeService {

    final CoinSubscribeRepository subscribeRepository;
    final CoinsRepository coinsRepository;

    private Coin subscribedCoin;

    @Autowired
    public SubscribeService(CoinSubscribeRepository subscribeRepository, CoinsRepository coinsRepository) {
        this.subscribeRepository = subscribeRepository;
        this.coinsRepository = coinsRepository;
    }


    public Coin setSingleSubscription(String coinCode) {
        Coin coin = coinsRepository.findByCommonCode(coinCode);
        if(coin != null) {
            subscribedCoin = coin;
        }
        return coin;
    }

    public Coin getSubscribedCoin() {
        if (subscribedCoin == null) {
            Iterator<Coin> iterator = coinsRepository.findAll().iterator();
            if(iterator.hasNext())
            subscribedCoin = iterator.next();
        }
        return subscribedCoin;
    }
}
