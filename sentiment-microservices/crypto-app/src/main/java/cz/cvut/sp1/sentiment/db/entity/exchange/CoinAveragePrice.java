package cz.cvut.sp1.sentiment.db.entity.exchange;

import cz.cvut.sp1.sentiment.db.entity.AbstractEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data @EqualsAndHashCode(callSuper = false)
@Entity @Builder
public class CoinAveragePrice extends AbstractEntity {


    @NotNull
    @ManyToOne(targetEntity = CoinPair.class)
    private CoinPair pair;
    @NotNull
    private BigDecimal price;

    private BigDecimal volume;
    private BigDecimal marketCapital;
    private Float change24h;

    @Temporal(value = TemporalType.TIMESTAMP)
    @NotNull
    private Date timestamp;
}
