package cz.cvut.sp1.sentiment.db;

import cz.cvut.sp1.sentiment.db.entity.exchange.CoinAveragePrice;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource(collectionResourceRel = "coin-history", path = "coin-history")
public interface CoinHistoryRepository extends PagingAndSortingRepository<CoinAveragePrice, Long> {
}
