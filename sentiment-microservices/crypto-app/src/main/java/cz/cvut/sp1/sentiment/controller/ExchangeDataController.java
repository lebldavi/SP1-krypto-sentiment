package cz.cvut.sp1.sentiment.controller;


import cz.cvut.sp1.sentiment.service.ExchangeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/exchange-data")
@Slf4j
public class ExchangeDataController {

    final ExchangeService service;

    @Autowired
    public ExchangeDataController(ExchangeService service) {
        this.service = service;
    }

    @GetMapping("/{coin}")
    public BigDecimal getCoinPairValue(@PathVariable("coin") String coin,
                               @RequestParam(value = "counter", defaultValue = "BTC") String counter) {
        log.info("GET : {} / {}", coin, counter);
        return service.coinTranslate(coin, counter);
    }

//    @GetMapping("/{coin}/{coinBase}")
//    public BigDecimal getCoinValueInBase(@PathVariable("coin") String coin,
//                                         @PathVariable("coinBase") String base) {
//        log.info("GET : {} / {}", coin, base);
//        return service.coinTranslate(coin, base);
//    }
}
