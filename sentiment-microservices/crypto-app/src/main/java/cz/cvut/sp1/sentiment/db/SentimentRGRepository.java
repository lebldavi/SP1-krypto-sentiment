package cz.cvut.sp1.sentiment.db;

import cz.cvut.sp1.sentiment.db.entity.sentiment.SentimentGroupResult;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import javax.xml.crypto.Data;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "sentiment-result", path = "sentiment-result")
public interface SentimentRGRepository extends PagingAndSortingRepository<SentimentGroupResult, Long> {

    List<SentimentGroupResult> findByStartStampGreaterThanEqual(Data data);
}
