package cz.cvut.sp1.sentiment.db.entity.sentiment;

import cz.cvut.sp1.sentiment.db.entity.AbstractEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

@Entity
@Data @EqualsAndHashCode(callSuper = false)
public class SentimentGroupResult extends AbstractEntity {

    //graph X
    @Temporal(TemporalType.TIMESTAMP)
    private Date startStamp;
    @Temporal(TemporalType.TIMESTAMP)
    private Date endStamp;

    //graph Y
    private Integer totalMessages;

    //graph Z
    private Integer finalValue;


    //for detail
    @OneToMany(targetEntity = Sentiment.class)
    private List<Sentiment> sentimentList;


    public SentimentGroupResult() {
        //JPA
    }

}
