package cz.cvut.sp1.sentiment.db.entity.exchange;

import cz.cvut.sp1.sentiment.db.entity.AbstractEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;


@EqualsAndHashCode(callSuper = true)
@Data @Builder
@Entity
public class CoinTicker extends AbstractEntity {

    @ManyToOne(targetEntity = CoinPair.class)
    @NotNull
    private CoinPair pair;

    @ManyToOne(targetEntity = ExchangeInfo.class)
    private ExchangeInfo exchange;

    private BigDecimal open;
    private BigDecimal close;
    private BigDecimal high;
    private BigDecimal low;

    private BigDecimal volume;
    private BigDecimal counterVolume;

    @Temporal(value = TemporalType.TIMESTAMP)
    @NotNull
    private Date timestamp;


}
