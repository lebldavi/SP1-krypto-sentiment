package cz.cvut.sp1.sentiment.service;

import lombok.extern.slf4j.Slf4j;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.binance.BinanceExchange;
import org.knowm.xchange.binance.dto.marketdata.BinancePrice;
import org.knowm.xchange.currency.CurrencyPair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;


@Service
@Slf4j
public class ExchangeService {

    ExchangeData primaryPuller;
    ExchangeData secondaryPuller;

    final BinanceDataPuller binanceDataPuller;

    @Autowired
    public ExchangeService(BinanceDataPuller binanceDataPuller) {
        this.binanceDataPuller = binanceDataPuller;
    }

    public BigDecimal getCoinValue(String coin, String counter) {
        return null;
    }
    public BigDecimal coinTranslate(String from, String to)
    {
        BigDecimal count;
        BigDecimal fromPrice=new BigDecimal(1);
        BigDecimal toPrice=new BigDecimal(1);
        String fromCurrency= "";
        String toCurrency= "";
        List<BinancePrice> prices = binanceDataPuller.getPrices();
        for (BinancePrice price : prices) {
            price.getCurrencyPair().counter.getCurrencyCode();
            if (price.getCurrencyPair().base.getCurrencyCode().equals(from)) {
                fromCurrency=price.getCurrencyPair().counter.getCurrencyCode();//todo this way
                fromPrice=price.getPrice();
                log.info("form price: {}", fromPrice);
            }
            if(price.getCurrencyPair().base.getCurrencyCode().equals(to)) {//todo
                toCurrency=price.getCurrencyPair().counter.getCurrencyCode();
                toPrice=price.getPrice();
                log.info("toPrice: {}", toPrice);
            }
        }
        if(fromCurrency.equals(toCurrency) && !fromCurrency.equals(""))
        {
            count=fromPrice.divide(toPrice,2);
            log.info("resoult: {}", count);
            return count;
        }
        return new BigDecimal(-1);

    }
}
