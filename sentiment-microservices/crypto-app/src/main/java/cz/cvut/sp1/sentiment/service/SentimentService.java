package cz.cvut.sp1.sentiment.service;


import cz.cvut.sp1.sentiment.db.CoinsRepository;
import cz.cvut.sp1.sentiment.db.SentimentsRepository;
import cz.cvut.sp1.sentiment.db.entity.exchange.Coin;
import cz.cvut.sp1.sentiment.db.entity.sentiment.Sentiment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

@Service
public class SentimentService {

    final CoinsRepository coinsRepository;
    final SentimentsRepository sentimentsRepository;

    @Autowired
    public SentimentService(CoinsRepository coinsRepository, SentimentsRepository sentimentsRepository) {
        this.coinsRepository = coinsRepository;
        this.sentimentsRepository = sentimentsRepository;
    }

    public void addSentimentBy(String coinCode, Sentiment sentiment) {
        Coin coin = coinsRepository.findByCommonCode(coinCode);
        sentiment.setCoin(coin);
//        sentiment.setTimeStamp(Date.valueOf(LocalDateTime.now().toLocalDate()));
        sentiment.setTimeStamp(new Date());
        sentimentsRepository.save(sentiment);

        //todo přidat sentiment do sentiment-groupy
    }
}
