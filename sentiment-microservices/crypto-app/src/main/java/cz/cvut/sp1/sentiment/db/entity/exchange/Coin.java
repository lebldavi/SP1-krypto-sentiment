package cz.cvut.sp1.sentiment.db.entity.exchange;

import cz.cvut.sp1.sentiment.db.entity.AbstractEntity;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Getter @Setter
public class Coin extends AbstractEntity {

    @NotEmpty
    private String name;

    @Column(unique = true)
    @NotEmpty
    private String commonCode;

    private String alternativeCode;

    private String icon;

    @ElementCollection(targetClass = String.class)
    private List<String> hashtags;

    public Coin() {
        //JPA
    }

    public Coin(@NotNull @Size(min = 1) String name, String commonCode, String icon) {
        this.name = name;
        this.commonCode = commonCode;
        this.icon = icon;
    }

    public Coin(@NotEmpty String name, @NotEmpty String commonCode, List<String> hashtags) {
        this.name = name;
        this.commonCode = commonCode;
        this.hashtags = hashtags;
    }

    @Override
    public String toString() {
        return "Coin{" +
                "name='" + name + '\'' +
                ", commonCode='" + commonCode + '\'' +
                '}';
    }
}

