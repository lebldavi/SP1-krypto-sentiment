package cz.cvut.sp1.sentiment.db;

import cz.cvut.sp1.sentiment.db.entity.exchange.Coin;
import cz.cvut.sp1.sentiment.db.entity.exchange.CoinSubscribe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "subscribe-coin", path = "subscribe-coin")
public interface CoinSubscribeRepository extends JpaRepository<CoinSubscribe, Long> {

}
