package cz.cvut.sp1.sentiment.service;

import java.io.IOException;

import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.bittrex.dto.marketdata.BittrexTicker;
import org.knowm.xchange.bittrex.service.BittrexMarketDataService;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

 
@Component
public class BittrexDataPuller implements ExchangeData {
    private static final Logger log = LoggerFactory.getLogger(BittrexDataPuller.class);

    private Exchange bittrex;
    private BittrexMarketDataService marketDataService;
    //private Set<BittrexTicker> bittrexSet = new LinkedHashSet<>();

    public BittrexDataPuller() {
        this.bittrex = ExchangeFactory.INSTANCE.createExchange(BittrexExchange.class);
        marketDataService = (BittrexMarketDataService)  bittrex.getMarketDataService();
    }

    //every 5 minute rate
    @Scheduled(initialDelay = 5000, fixedRate = 300_000)
    private void pullTickers() throws IOException {
                BittrexTicker ticket = marketDataService.getBittrexTicker( new CurrencyPair(Currency.BTC, Currency.USDT));
                //bittrexSet.add(ticket);
                log.info("pullTickers() ====> " + ticket.toString());
    }
}
