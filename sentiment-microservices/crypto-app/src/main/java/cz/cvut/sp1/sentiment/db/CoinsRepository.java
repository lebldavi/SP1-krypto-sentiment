package cz.cvut.sp1.sentiment.db;

import cz.cvut.sp1.sentiment.db.entity.exchange.Coin;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "coin", path = "coin")
public interface CoinsRepository extends PagingAndSortingRepository<Coin, Long> {

    Coin findByCommonCode(String commonCode);

    Coin findByName(String name);
}
