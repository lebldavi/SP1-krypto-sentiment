package cz.cvut.sp1.sentiment.db;

import cz.cvut.sp1.sentiment.db.entity.sentiment.Sentiment;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "sentiment", path = "sentiment")
public interface SentimentsRepository extends PagingAndSortingRepository<Sentiment, Long> {
//
//    @GetMapping("/single/{coin}")
//    List<Sentiment> findAllByCoin_CommonCode(@PathVariable("coin") String commonCode);

    List<Sentiment> findAllByCoin_CommonCode(@Param("coinCode") String commonCode);

    List<Sentiment> findAllByOrderByIdDesc();

}
