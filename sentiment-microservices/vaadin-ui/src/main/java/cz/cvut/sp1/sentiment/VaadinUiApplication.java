package cz.cvut.sp1.sentiment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.hateoas.config.EnableHypermediaSupport;

@EnableDiscoveryClient
@EnableCircuitBreaker
@EnableFeignClients
@SpringBootApplication(scanBasePackages = { "cz.cvut.sp1.sentiment" , "cz.cvut.sp1.sentiment.client" })
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class VaadinUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(VaadinUiApplication.class, args);
	}
}
