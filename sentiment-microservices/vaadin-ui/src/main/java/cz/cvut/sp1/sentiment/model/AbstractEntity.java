package cz.cvut.sp1.sentiment.model;


import java.io.Serializable;
import java.util.Objects;

public abstract class AbstractEntity implements Serializable{

    private Long id;

    private int version;

    public boolean isNew() {
        return id == null;
    }

    public Long getId() {
        return id;
    }

    public int getVersion() {
        return version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractEntity that = (AbstractEntity) o;
        return version == that.version &&
                Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, version);
    }
}
