package cz.cvut.sp1.sentiment.client;

import cz.cvut.sp1.sentiment.model.Coin;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class CoinServiceFallback implements CoinService {
    @Override
    public Resources<Coin> findAll() {
        return new Resources<Coin>(Collections.singletonList(new Coin("Empty", "EMP", null)));
    }
}
