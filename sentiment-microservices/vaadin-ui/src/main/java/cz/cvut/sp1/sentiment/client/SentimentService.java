package cz.cvut.sp1.sentiment.client;

import cz.cvut.sp1.sentiment.model.Sentiment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Primary;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient(value = "crypto-app")
@Primary
public interface SentimentService {

    @GetMapping("/sentiment-data/{coin}")
    List<Sentiment> getByCoin(@PathVariable("coin") String coin);

    @GetMapping("/sentiment-data")
    List<Sentiment> getLatest();

    @GetMapping("/sentiment")
    List<Sentiment> getAll();

}
