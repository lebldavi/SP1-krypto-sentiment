package cz.cvut.sp1.sentiment.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@EqualsAndHashCode(callSuper = true)
@Data
public class Coin extends AbstractEntity {

    private String name;

    private String commonCode;

    private String icon;

    public Coin() {
        //JPA
    }

    public Coin(String name, String commonCode, String icon) {
        this.name = name;
        this.commonCode = commonCode;
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "" + name + " - " + commonCode;
    }
}

