package cz.cvut.sp1.sentiment.client;

import cz.cvut.sp1.sentiment.model.Coin;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Primary;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(value = "crypto-app", fallback = CoinServiceFallback.class)
@Primary
public interface CoinService {

    @GetMapping("/coin")
    Resources<Coin> findAll();
}
