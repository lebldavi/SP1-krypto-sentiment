package cz.cvut.sp1.sentiment.view;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.viritin.label.Header;

@SpringView(name = CoinView.VIEW_NAME)
public class CoinView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "coins";

    public CoinView() {
        addComponent(new Header("Přehled coinů"));
    }


}
