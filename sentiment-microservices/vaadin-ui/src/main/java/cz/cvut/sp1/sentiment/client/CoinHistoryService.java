package cz.cvut.sp1.sentiment.client;

import cz.cvut.sp1.sentiment.model.CoinMarketInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.HttpHeaders.USER_AGENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Primary
@FeignClient(name = "coincap-history", url = "http://coincap.io", fallback = CoinHistoryFallback.class)
@RequestMapping(value = "/history", headers = {USER_AGENT + "=Mozilla/5.0", ACCEPT + "=" + APPLICATION_JSON_VALUE})
public interface CoinHistoryService {

    @GetMapping("/{coin}")
    CoinMarketInfo getAllHistory(@PathVariable("coin") String coin);

    @GetMapping("/{cnt}day/{coin}")
    CoinMarketInfo getDayHistory(@PathVariable("cnt") String cnt, @PathVariable("coin") String coin);



    @GetMapping("/1day/{coin}")
    CoinMarketInfo get1DayHistory(@PathVariable("coin") String coin);

    @GetMapping("/7day/{coin}")
    CoinMarketInfo get7DayHistory(@PathVariable("coin") String coin);

    @GetMapping("/30day/{coin}")
    CoinMarketInfo get30DayHistory(@PathVariable("coin") String coin);

    @GetMapping("/90day/{coin}")
    CoinMarketInfo get90DayHistory(@PathVariable("coin") String coin);

    @GetMapping("/180day/{coin}")
    CoinMarketInfo get180DayHistory(@PathVariable("coin") String coin);

    @GetMapping("/365day/{coin}")
    CoinMarketInfo getYearHistory(@PathVariable("coin") String coin);
}
