package cz.cvut.sp1.sentiment.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data @EqualsAndHashCode(callSuper = false)
public class Sentiment extends AbstractEntity {

    private Double sentimentValue = 0.0;

    private Date timeStamp;

    private String tweetMessage;
    private Coin coin;

    public Sentiment() {
        //JPA
    }

    public Sentiment(Double sentimentValue, Date timeStamp, String tweetMessage, Coin coin) {
        this.sentimentValue = sentimentValue;
        this.timeStamp = timeStamp;
        this.tweetMessage = tweetMessage;
        this.coin = coin;
    }
}
