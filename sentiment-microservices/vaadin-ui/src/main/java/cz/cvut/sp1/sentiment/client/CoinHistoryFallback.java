package cz.cvut.sp1.sentiment.client;

import cz.cvut.sp1.sentiment.model.CoinMarketInfo;
import org.springframework.stereotype.Service;

public class CoinHistoryFallback implements CoinHistoryService {
    @Override
    public CoinMarketInfo getDayHistory(String cnt, String coin) {
        return null;
    }

    @Override
    public CoinMarketInfo getAllHistory(String coin) {
        return null;
    }

    @Override
    public CoinMarketInfo get1DayHistory(String coin) {
        return null;
    }

    @Override
    public CoinMarketInfo get7DayHistory(String coin) {
        return null;
    }

    @Override
    public CoinMarketInfo get30DayHistory(String coin) {
        return null;
    }

    @Override
    public CoinMarketInfo get90DayHistory(String coin) {
        return null;
    }

    @Override
    public CoinMarketInfo get180DayHistory(String coin) {
        return null;
    }

    @Override
    public CoinMarketInfo getYearHistory(String coin) {
        return null;
    }
}
