package cz.cvut.sp1.sentiment.model;


import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class SentimentGroupResult extends AbstractEntity {

    //graph X
    private Data startStamp;
    private Data endStamp;

    //graph Y
    private Integer totalMessages;

    //graph Z
    private Integer finalValue;


    //for detail
    private List<Sentiment> sentimentList;


    public SentimentGroupResult() {
        //JPA
    }

}
