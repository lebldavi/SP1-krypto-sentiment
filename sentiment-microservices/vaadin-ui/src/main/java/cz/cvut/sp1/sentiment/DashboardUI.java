package cz.cvut.sp1.sentiment;

import com.byteowls.vaadin.chartjs.ChartJs;
import com.byteowls.vaadin.chartjs.config.*;
import com.byteowls.vaadin.chartjs.data.*;
import com.byteowls.vaadin.chartjs.options.InteractionMode;
import com.byteowls.vaadin.chartjs.options.Position;
import com.byteowls.vaadin.chartjs.options.scale.*;
import com.byteowls.vaadin.chartjs.utils.ColorUtils;
import com.vaadin.annotations.Title;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import cz.cvut.sp1.sentiment.client.CoinHistoryService;
import cz.cvut.sp1.sentiment.client.CoinService;
import cz.cvut.sp1.sentiment.client.SentimentService;
import cz.cvut.sp1.sentiment.model.Coin;
import cz.cvut.sp1.sentiment.model.CoinMarketInfo;
import cz.cvut.sp1.sentiment.model.Sentiment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.addon.twitter.Timeline;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.label.MLabel;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MPanel;
import org.vaadin.viritin.layouts.MVerticalLayout;
import org.vaadin.viritin.layouts.MWindow;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.*;

@SpringUI
@Title("Crypto Sentiment")
@SuppressWarnings({"serial", "message"})

public class DashboardUI extends UI {
    private static final Logger log = LoggerFactory.getLogger(DashboardUI.class);

    private final CoinService coinService;
    private final CoinHistoryService historyService;
    private final SentimentService sentimentService;

    private final ComboBox<Coin> coinComboBox;
    private List<Dataset<?, ?>> marketDatasets;
    private final ChartJs marketChart;
    private LineChartConfig marketChartConfig;
    private BubbleChartConfig bubbleChartConfig;
    private SimpleDateFormat dateFormat;

    public static final int[][] RGB_ARR = {{255, 99, 132}, {54, 172, 235}, {54, 235, 172}}; //[0]=red, [1]=blue [2]=green
    private final ChartJs bubbleChart;
    private final ChartJs barChart;
    private BarChartConfig barChartConfig;


    @Autowired
    public DashboardUI(CoinService coinService, CoinHistoryService historyService, SentimentService sentimentService) {
        this.coinService = coinService;
        this.historyService = historyService;


        VerticalLayout content = new MVerticalLayout();

        MHorizontalLayout topBar = new MHorizontalLayout().withFullWidth()
                .withDefaultComponentAlignment(Alignment.MIDDLE_LEFT);
        content.addComponent(topBar);

        MLabel header = new MLabel("Dashboard")
                .withStyleName(ValoTheme.LABEL_H1 + " " + ValoTheme.LABEL_NO_MARGIN);
        MButton mainBtn = new MButton("Hlavní")
                .withStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED)
                .withStyleName(ValoTheme.BUTTON_LARGE);
        MButton advanceBtn = new MButton("Pokročilý")
                .withStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED)
                .withStyleName(ValoTheme.BUTTON_LARGE);
        MButton settingBtn = new MButton(VaadinIcons.COG);
        MButton compareBtn = new MButton("Porovnání")
                .withStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED)
                .withStyleName(ValoTheme.BUTTON_LARGE);
        topBar.with(header, mainBtn, advanceBtn, compareBtn)
                .add(settingBtn, Alignment.MIDDLE_RIGHT);

        //space separator
        content.addComponent(new MLabel(""));
        
        
        
        MHorizontalLayout layout = new MHorizontalLayout().withFullSize();
        content.addComponent(layout);

        coinComboBox = new ComboBox<>("Vyber coin");
        coinComboBox.setIcon(VaadinIcons.COINS);
        coinComboBox.setWidth(15f, Unit.EM);
        coinComboBox.addStyleName(ValoTheme.COMBOBOX_LARGE);
        coinComboBox.setItemCaptionGenerator(Coin::toString);
        coinComboBox.setEmptySelectionAllowed(false);


        MTextField search = new MTextField("filter")
                .withPlaceholder("bitcoin to the moon")
                .withIcon(VaadinIcons.TWITTER)
                .withFullWidth();
        MVerticalLayout newsLayout = new MVerticalLayout().withFullWidth().withMargin(false)
                .with(search)
                .expand(
                        Timeline.widget("989635203248386048"));
//                        Timeline.url("https://twitter.com/hashtag/TwitterStories"));

        bubbleChart = getBubbleChart();
        bubbleChart.addClickListener(this::onChartBubbleClick);
        marketChart = getMarketChart();

        MButton hourBtn = new MButton("h");
        MButton dayBtn = new MButton("d");
        MButton weekBtn = new MButton("w");
        MButton monthBtn = new MButton("m");
        MButton yearBtn = new MButton("y");
        MButton allBtn = new MButton("all");
        CssLayout chartSettBtnWraper = new CssLayout(hourBtn, dayBtn, weekBtn, monthBtn, yearBtn, allBtn);
        chartSettBtnWraper.addStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);

        MHorizontalLayout chartToolbar = new MHorizontalLayout().withFullWidth()
                .add(coinComboBox)
                .add(chartSettBtnWraper, Alignment.BOTTOM_RIGHT);

        barChart = getBarChart();

        MHorizontalLayout sentimentChartWrap = new MHorizontalLayout().withFullWidth()
                .add(bubbleChart, 1.5f)
                .expand(barChart);

        MVerticalLayout chartsWrap = new MVerticalLayout().withMargin(false).withFullWidth()
                .add(chartToolbar)
                .expand(new MPanel(sentimentChartWrap),
                        new MPanel(marketChart));

        layout.add(chartsWrap, 70);
        layout.add(newsLayout, 30);


        setContent(content);
        this.sentimentService = sentimentService;
    }

    @PostConstruct
    void init() {
        coinComboBox.addValueChangeListener(valueChangeEvent -> {
            CoinMarketInfo dayHistory = historyService.getYearHistory(valueChangeEvent.getValue().getCommonCode());
            dayHistory.setCoinCounter(valueChangeEvent.getValue().getCommonCode());
            dayHistory.setBase("USD");
//            log.info("{} history: {}", dayHistory.getCoinCounter(), dayHistory.getPrice().get(0));
            updateMarketData(dayHistory);
        });
        coinComboBox.addValueChangeListener(valueChangeEvent -> {
            List<Sentiment> sentimentList = sentimentService.getByCoin(valueChangeEvent.getValue().getCommonCode());
            log.info("SentimentList: {}", sentimentList);
            updateSentimentData(sentimentList);
            updateBarChart(sentimentList);
        });



        Collection<Coin> coins = coinService.findAll().getContent();
        coinComboBox.setItems(coins);
        if(coins.iterator().hasNext())
            coinComboBox.setSelectedItem(coins.iterator().next());
    }





    private ChartJs getBubbleChart() {
        bubbleChartConfig = new BubbleChartConfig();
        bubbleChartConfig.
                data()
                .addDataset(new BubbleDataset().label("none"))
                .and()
                .options()
                .responsive(true)
                .title()
                .display(true)
                .text("Sentiment")
                .and()
                .done();

//        Random rand = new Random();
//        int dtIndex=0;
//        for (Dataset<?, ?> ds : bubbleChartConfig.data().getDatasets()) {
//            BubbleDataset bds = (BubbleDataset) ds;
//            bds.backgroundColor(ColorUtils.toRgba(RGB_ARR[dtIndex], 0.7));
//            for (int i = 0; i < 15; i++) {
//                bds.addData(rand.nextDouble()*100*Math.pow(-1.0, i),
//                          rand.nextDouble()*100*Math.pow(-1.0, i),
//                          rand.nextDouble()*100 / 5);
//            }
//            dtIndex++;
//        }

        ChartJs chart = new ChartJs(bubbleChartConfig);
        chart.setJsLoggingEnabled(true);
        chart.setWidth("100%");
        return chart;
    }

    private void updateSentimentData(List<Sentiment> sentiments) {
        SimpleDateFormat format = new SimpleDateFormat("h:m:s");
        List<Dataset<?, ?>> datasets = bubbleChartConfig.data().getDatasets();

        BubbleDataset negativ = new BubbleDataset().label("Negativ")
                .backgroundColor(ColorUtils.toRgba(RGB_ARR[0], 0.7));;
        BubbleDataset positive = new BubbleDataset().label("Positive")
                .backgroundColor(ColorUtils.toRgba(RGB_ARR[2], 0.7));;

        datasets.clear();
        datasets.add(positive);
        datasets.add(negativ);

        fillSentimentDatasets(sentiments, negativ, positive);


        bubbleChart.refreshData();
    }

    private void fillSentimentDatasets(List<Sentiment> sentiments, BubbleDataset negativ, BubbleDataset positive) {
        Random rand = new Random();
        sentiments.forEach(sentiment -> {
            BubbleData bubbleData = new BubbleData()
                    .x((double) sentiment.getTimeStamp().getTime()/10_000)
                    .y(rand.nextDouble()*100)
                    .r(sentiment.getSentimentValue()*100/5);
//            log.info("buble-data: {}", bubbleData.toString());
            if(sentiment.getSentimentValue()>=0)
                positive.addData((double) sentiment.getTimeStamp().getTime()/10_000,
                        rand.nextDouble() * 100,
                        sentiment.getSentimentValue()*10);
            else //(sentiment.getSentimentValue()<0)
                negativ.addData((double) sentiment.getTimeStamp().getTime()/10_000,
                        rand.nextDouble() * 100,
                        sentiment.getSentimentValue()*10*(-1));
        });
    }

    private ChartJs getMarketChart() {
        int[][] RGB_ARR = {{255, 99, 132}, {54, 172, 235}}; //[0]=red, [1]=blue

        marketChartConfig = new LineChartConfig();
        marketChartConfig.data()
//                .labels("January", "February", "March", "April", "May", "June", "July")
                .extractLabelsFromDataset(true)
                .addDataset(new LineDataset().label("NONE")
                        .borderColor(ColorUtils.toRgba(RGB_ARR[1], 1.0))
                        .backgroundColor(ColorUtils.toRgba(RGB_ARR[1], 0.7)))
                .and()
                .options()
                .responsive(true)
                .title()
                .display(true)
                .text("Market prices")
                .and()
                .tooltips()
                .mode(InteractionMode.INDEX)
                .and()
                .hover()
                .mode(InteractionMode.INDEX)
                .and()
                .scales()
                .add(Axis.X, new CategoryScale()
                        .scaleLabel()
                        .display(true)
                        .labelString("Days")
                        .and())
                .add(Axis.Y, new LinearScale()
                        .stacked(true)
                        .scaleLabel()
                        .display(true)
                        .labelString("Value")
                        .and())
                .and()
                .done();

        // add random data for demo
        List<String> labels = marketChartConfig.data().getLabels();
        marketDatasets = marketChartConfig.data().getDatasets();
//        for (Dataset<?, ?> ds : marketDatasets) {
//            LineDataset lds = (LineDataset) ds;
//            List<Double> data = new ArrayList<>();
//            for (int i = 0; i < labels.size(); i++) {
//                data.add((double) Math.round(Math.random() * 100));
//            }
//            lds.dataAsList(data);
//        }

        ChartJs chart = new ChartJs(marketChartConfig);
        chart.setJsLoggingEnabled(true);
        chart.setWidth("100%");
        return chart;
    }

    private void updateMarketData(CoinMarketInfo coins) {
        dateFormat = new SimpleDateFormat("d.M.yy");
        String label = String.format("%s / %s", coins.getCoinCounter(), coins.getBase());

//        Dataset<?, ?> dataset = marketDatasets.get(0);
//        LineDataset lds = (LineDataset) dataset;
//        List<Double> data = coins.getPrice().stream()
//                .map(value -> value[1])
//                .collect(Collectors.toList());
//        lds.dataAsList(data);
//
        LineDataset ds = new LineDataset().label(label)
                .borderColor(ColorUtils.toRgba(RGB_ARR[1], 1.0))
                .backgroundColor(ColorUtils.toRgba(RGB_ARR[1], 0.7));

        marketDatasets.clear();
        marketDatasets.add(ds);

        List<Double[]> prices = coins.getPrice();
        for (int i = 0; i < prices.size()-2; i++) {     // -2 -> aby to nepřidalo 2 stejný poslední data
            ds.addLabeledData(dateFormat.format(new Date(prices.get(i)[0].longValue())), prices.get(i)[1]);

        }
//        coins.getPrice().forEach(c -> {
//            ds.addLabeledData(format.format(new Date(c[0].longValue())), c[1]);
//        });



        marketChart.refreshData();
    }


    @Override
    protected void init(VaadinRequest vaadinRequest) {

    }

    public ChartJs getBarChart() {
        barChartConfig = new BarChartConfig();
        barChartConfig.data()
                .labels("total")
                .addDataset(new BarDataset()
                        .label("Negative")
                        .backgroundColor(ColorUtils.toRgba(RGB_ARR[0], 0.7))
                        .stack("Stack 0"))
                .addDataset(new BarDataset()
                        .label("Positive")
                        .backgroundColor(ColorUtils.toRgba(RGB_ARR[1], 0.7))
                        .stack("Stack 0"))
                .and()
                .options()
                .responsive(true)
                .title()
                .display(true)
                .text("Total value")
                .and()
                .tooltips()
                .mode(InteractionMode.INDEX)
                .intersect(false)
                .and()
                .scales()
                .add(Axis.X, new DefaultScale()
                        .stacked(true))
                .add(Axis.Y, new DefaultScale()
                        .stacked(true))
                .and()
                .done();

        ChartJs chart = new ChartJs(barChartConfig);
        chart.setJsLoggingEnabled(true);
//        chart.setWidth("100%");
        return chart;

    }

    public void updateBarChart(List<Sentiment> sentiments) {
        BarDataset negative = new BarDataset()
                .label("Negative")
                .backgroundColor(ColorUtils.toRgba(RGB_ARR[0], 1))
                .stack("Stack 0");
        BarDataset positive = new BarDataset()
                .label("Positive")
                .backgroundColor(ColorUtils.toRgba(RGB_ARR[2], 1))
                .stack("Stack 0");

        List<Dataset<?, ?>> datasets = barChartConfig.data().getDatasets();
        datasets.clear();
        datasets.add(negative);
        datasets.add(positive);

        Double pos = 0.0, neg=0.0;
        for (Sentiment sentiment : sentiments) {
            if(sentiment.getSentimentValue()>=0)
                pos+=sentiment.getSentimentValue();
            else
                neg+=sentiment.getSentimentValue();
        }

        positive.addData(pos);
        negative.addData(neg);

        barChart.refreshData();
    }

    public ChartJs getRadarChart() {
        RadarChartConfig config = new RadarChartConfig();
        int[] RGB_ARR_RED = {255, 99, 132};
        int[] RGB_ARR_BLUE = {54, 162, 235};
        config
                .data()
                .labels("single", "replay", "retweet", "like", "mention", "followers", "guru-level")
                .addDataset(new RadarDataset().label("Negative")
                        .backgroundColor(ColorUtils.toRgba(RGB_ARR_RED, 0.2))
                        .borderColor(ColorUtils.toRgb(RGB_ARR_RED))
                        .pointBackgroundColor(ColorUtils.toRgb(RGB_ARR_RED)))
                .addDataset(new RadarDataset().label("Positive")
                        .backgroundColor(ColorUtils.toRgba(RGB_ARR_BLUE, 0.2))
                        .borderColor(ColorUtils.toRgb(RGB_ARR_BLUE))
                        .pointBackgroundColor(ColorUtils.toRgb(RGB_ARR_BLUE)))
                .and();

        config.
                options()
                .legend()
                .position(Position.TOP)
                .and()
                .title()
                .display(true)
                .text("Sentiment - podrobnosti")
                .and()
                .scale(new RadialLinearScale()
                        .ticks()
                        .beginAtZero(true)
                        .and()
                        .gridLines()
                        .circular(true)
                        .and()
                        .pointLabels()
                        .display(true)
                        .and()
                        .angleLines()
                        .display(true)
                        .and()
                )
                .done();

        List<String> labels = config.data().getLabels();
        for (Dataset<?, ?> ds : config.data().getDatasets()) {
            RadarDataset lds = (RadarDataset) ds;
            List<Double> data = new ArrayList<>();
            for (int i = 0; i < labels.size(); i++) {
                data.add((double) (Math.round(Math.random() * 100)));
            }
            lds.dataAsList(data);
        }

        ChartJs chart = new ChartJs(config);
        chart.setJsLoggingEnabled(true);
        return chart;
    }

    private void onChartBubbleClick(int i, int i1) {
        MWindow window = new MWindow("Totally Random :)")
                .withCenter();
        window.setContent(new MVerticalLayout(getRadarChart()));

        getUI().addWindow(window);
    }
}
