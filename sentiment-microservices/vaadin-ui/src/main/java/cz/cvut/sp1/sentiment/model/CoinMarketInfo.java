package cz.cvut.sp1.sentiment.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class CoinMarketInfo {

    @JsonIgnore
    private String coinCounter;
    @JsonIgnore
    private String base;

    @JsonProperty("market_cap")
    private List<Long[]> marketCap;

    @JsonProperty("price")
    private List<Double[]> price;

    @JsonProperty("volume")
    private List<Long[]> volume;

}
