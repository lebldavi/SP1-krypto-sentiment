# Sentiment - Spring Cloud Data Flow + Elastic, Kibana


## Architektura a aplikace
Aplikace beží v Dockeru a jsou deploynutý přez SCDF (pro nasazení je potřeba použít Kubernetes Server, nebo Cloud Foundry Server). Pro streamy je použit message broker Kafka (lze přenastavit na RabbitMQ).

Seznam aplikací:
* twitter-setream - "source" příjmajcí tweety podle konfigurace v twitter-setting-properties
* filter - "processor" filtrující tweety od podvodných uživatelů a bottů
* twitter-sentiment - "processor" vyhodocující sentiment příchozího tweetu (TensorFlow)
* field-value-counter & aggregate-counter - "sink" pro jednoduchou analýzu
* log - "sink" loguje příchozí zprávy do souboru

mimo SCDF: (přímé napojení na kafka topic)
* sentiment-tap,  **Elastic** - kafka topic, který je zpracovávaný logstashem a posílaná do Elasticsearche



 ![streams](uploads/dataflow_diagram.png)
 ![kibana](uploads/kibana_dashboard.png)

**Požadavky:**
 * Docker + docker-compose https://www.docker.com
 * POZOR na použití verzí Kafka +1.1 a SpringBoot +2.0 aplikace, při použítí nižších verzí je header ukládám dovnit message. (až s příchodem kafka 1.1 je podpora odděleného headru.). Spring Cloud Stream Aplikace v docker-compose podorují pouze starší kafku bez headru.

## Spuštění a propojení aplikací

**Spuštění local dataflow-servru**  (aktuálně i elastic, kibana, logstash)
```shell
cd ./dataflow
# DATAFLOW_VERSION=1.7.2.RELEASE
docker-compose up -d       # -d = na pozadí
```

**(Spuštění Elastic a Kibana) TODO** - rozdělit zvlášt a propejit konteinery (stejný network)
```shell
cd ./elastic-kibana
docker-compose up
```

otevření containeru ve kterém běží dataflow-server
```shell
docker exec -it dataflow-server bash
```

**vytvoření basic streamu s napojením na Elastic pomocí logstash** (shell)
```shell
#spustíme dataflow-shell, který se připojuje k dataflow-servru
java -jar dataflow-shell.jar

#registrujeme naší upravenou aplikace twitter-sentiment
app register --name twitter-sentiment --type processor --uri file://apps/twitter-sentiment-processor-kafka-2.0.3.RELEASE.jar --metadata-uri file://apps/twitter-sentiment-processor-kafka-2.0.3.RELEASE-metadata.jar --force

#vytvoříme stream, výstupní kafka-topic = sentiment-tap
stream create --name sentiment --definition "twitterstream | twitter-sentiment > :sentiment-tap"

#spustíme stream
stream deploy --name sentiment --propertiesFile tweet-sentiment.properties
```
 * po spuštení přídáme v Elasticku (Kibane) index, a importujeme dashboard
 * **TODO** jak na to...

![kibana_tweets](uploads/kibana_tweets.png)


## Další stremy v SCDF 
(dashboardu http://localhost:9393/dashboard/#/apps)

vytvoříme stream zdroje(source) tweetů, s konfigurací v tweets.properties
```
tweets = twitterstream | filter --expression='#jsonPath(payload,"$.retweeted")==false && #jsonPath(payload,"$.user.followers_count")>20 && #jsonPath(payload,"$.user.friends_count")>20' | log

sentiment=:tweets.filter > twitter-sentiment --vocabulary='http://dl.bintray.com/big-data/generic/vocab.csv' --output-name=output/Softmax --model='http://dl.bintray.com/big-data/generic/minimal_graph.proto' --model-fetch=output/Softmax | log

sentiment-counter=:tweets.twitter-sentiment > field-value-counter --field-name=sentiment --name=sentiment
tweets-volume = :tweets.filter > aggregate-counter --name=volumeCounter --date-format="EEE MMM dd HH:mm:ss Z yyyy" --time-field='#jsonPath(payload,"$.created_at")'
```

**vytvoření streamů** (shell)
```shell
#spustíme dataflow-shell, který se připojuje k dataflow-servru
java -jar dataflow-shell.jar

#vytovříme twitter streamu
stream create  --name tweets --definition "twitterstream | filter | log"
stream deploy --name tweets --propertiesFile tweets.properties

#sentiment processor
stream create --name sentiment --definition ":tweets.filter > twitter-sentiment"
stream deploy --name sentiment --propertiesFile sentiment.properties

#případně(zatím) jednoduché zobrazení
stream create --definition 'tweets.filter > aggregate-counter --name=volumeCounter --date-format="EEE MMM dd HH:mm:ss Z yyyy" --time-field=payload.created_at)' --name volume-counter --deploy
sentiment-counter=:tweets.twitter-sentiment > field-value-counter --field-name=sentiment --name=sentiment --deploy
```

# Deploy na Google Cloud Platform - Kubernetes

 ![kubernetes](uploads/kubernetes_pods.png)

**Instalace na Kubernetes:**
```shell
git clone https://gitlab.fit.cvut.cz/lebldavi/SP1-krypto-sentiment.git
cd SP1-krypto-sentiment/


cd dataflow/kubernetes/

#kafka, mysql, redis
kubectl create -f ./kafka/
kubectl create -f ./mysql/
kubectl create -f ./redis/

kubectl create -f ./metrics/metrics-svc.yaml

#skipper
kubectl create -f ./skipper/skipper-deployment.yaml
kubectl create -f ./skipper/skipper-svc.yaml


#SCDF server
    #1. cluster-admin !!!!!!
    kubectl create clusterrolebinding myname-cluster-admin-binding \
    --clusterrole=cluster-admin \
    --user={example@email.com} # <---- !!
    #2.role binding
    kubectl create -f ./server/server-roles.yaml
    kubectl create -f ./server/server-rolebinding.yaml
    kubectl create -f ./server/service-account.yaml
# ...můžem spustit
kubectl create -f ./server/server-svc.yaml
kubectl create -f ./server/server-deployment.yaml


#kibana, logstash, elasticsearch
kubectl create -f ./kibana/
kubectl create -f ./logstash/
#kubectl create -f ./elasticsearch/  # - aktuálně z google marketplace
```


**Připojení se k SCDF servru:**
```shell
kubectl get svc scdf-server
#NAME          TYPE           CLUSTER-IP     EXTERNAL-IP     PORT(S)        AGE
#scdf-server   LoadBalancer   10.39.248.53   35.225.212.60   80:30340/TCP   1m
```
Ze spušteného dataflow shellu nastavíme server:
```shell
dataflow config server --username user --password password --uri http://{EXTERNAL_IP}
#dataflow config server --username user --password password --uri http://35.225.212.60
```

Konfigurace a nahrání applikace na SCDF:
```shell
#nope, aplikace je potřeba nahrát jako docker image, #todo - Bulk import application -> http://bit.ly/Darwin-SR3-stream-applications-kafka-docker
wget -qO- 'http://{SCDF_SERVER_IP}/apps' --post-data='uri=https://repo.spring.io/libs-release/org/springframework/cloud/stream/app/spring-cloud-stream-app-descriptor/Darwin.SR3/spring-cloud-stream-app-descriptor-Darwin.SR3.kafka-apps-maven-repo-url.properties'

#dataflow shell - registrace našich aplikací
app register --name twitter-sentiment --type processor --uri docker:gitlab.fit.cvut.cz:5000/lebldavi/sp1-krypto-sentiment/apps/twitter-sentiment:2.0.3.RELEASE --metadata-uri maven://org.springframework.cloud.stream.app:twitter-sentiment-processor-kafka:jar:metadata:2.0.3.RELEASE --force
```
Naše aplikace musí být nahrány jako Docker Image, takže pokud nemáme image na Gitlabu nebo v DockerHubu:
```shell
cd {target_build_s_Dockerfile}
docker login gitlab.fit.cvut.cz:5000
docker build . -t gitlab.fit.cvut.cz:5000/lebldavi/sp1-krypto-sentiment/apps/twitter-sentiment:2.0.3.RELEASE
docker push gitlab.fit.cvut.cz:5000/lebldavi/sp1-krypto-sentiment/apps/twitter-sentiment:2.0.3.RELEASE

# image pak najdeme na adrese -> docker:gitlab.fit.cvut.cz:5000/lebldavi/sp1-krypto-sentiment/apps/twitter-sentiment:2.0.3.RELEASE
# podívat se můžeme přez GitLab registry -> https://gitlab.fit.cvut.cz/lebldavi/SP1-krypto-sentiment/container_registry
```

Konfigurace Kibany a logstashe
```shell
# Zjednodušeno přez ConfigMap !
# vytvoření konfigurace (volume) pro logstash pipeline, ConfigMap je pak připojena jako volume do logstashe
kubectl create configmap logstash --from-file=dataflow/kubernetes/logstash/sentiment-pipeline.conf
#kubectl get cm logstash
#kubectl get cm logstash -o yaml
```

Pomocné příkazy pri neúspěchu
```shell
kubectl delete all -l app={CONTAINER_NAME}
#smazat nastavení
kubectl delete configmap {CONTAINER_NAME}
#logy
kubectl logs -f {POD_NAME}
```

**Aplikace - streamy pak dále spustíme stejně jako na locálu**



## Zdroje a dokumentace k Spring Cloud Data Flow
http://cloud.spring.io/spring-cloud-dataflow/

* dokumentace DataFlow: https://docs.spring.io/spring-cloud-dataflow/docs/current/reference/htmlsingle/
* examples: https://docs.spring.io/spring-cloud-dataflow-samples/docs/current/reference/htmlsingle/#_streaming
* apps-starter doc: https://docs.spring.io/spring-cloud-stream-app-starters/docs/Celsius.SR2/reference/htmlsingle/
* apps-starter git: https://github.com/spring-cloud-stream-app-starters
* streams spring-boot: https://github.com/spring-cloud/spring-cloud-dataflow/blob/master/spring-cloud-dataflow-docs/src/main/asciidoc/streams.adoc#register-a-stream-app
* Spring Cloud Streams: https://cloud.spring.io/spring-cloud-stream/
* Docker-compose to Kubernetes: https://kubernetes.io/docs/tasks/configure-pod-container/translate-compose-kubernetes/
* SCDF kubernetes server: https://docs.spring.io/spring-cloud-dataflow-server-kubernetes/docs/current-SNAPSHOT/reference/htmlsingle/#getting-started-deploying-streams

Sentiment classification:
* TensorFlow sentiment git: https://github.com/danielegrattarola/twitter-sentiment-cnn
* způsob vyhodnocování: http://thinknook.com/twitter-sentiment-analysis-training-corpus-dataset-2012-09-22/
