import Queue
import math
import sys
import threading
import requests_oauthlib
from pyspark import SparkConf, SparkContext
from tweetDownloadThread import tweetDownloadThread
from postRestThread import postRestThread
import requests
import simplejson as json


# https://twitter.com/i/web/status/<ID>

# function to analyze tweet text and to determine its sentiment
def sparkAnalysisOfSentiment(tweetBuffer, sentimentBuffer, sc, afinn):
    while True:
        try:
	        full_tweet_tuple = None
	        # get tweet from buffer
	        if not tweetBuffer.empty():
	            full_tweet_tuple = tweetBuffer.get()
	        if full_tweet_tuple is None:
	            continue
	        full_tweet = full_tweet_tuple[1]
	        krypto_ID = full_tweet_tuple[0]
	        tweet_text = full_tweet['text']
	        tweet_id = full_tweet['id']

	        lst = [""]
	        # analyze using spark
	        lst[0] = tweet_text
	        text = sc.parallelize(lst)
	        words = text.flatMap(lambda line: line.split(" "))
	        wordsSentiment = words.map(lambda w: afinn.get(w, 0))
	       
	        # sum of sentiment of all words in tweet
	        mySum = wordsSentiment.sum()
	        # print "Sum of sentiments: " + str(mySum)
	        numOfWords = wordsSentiment.count()
	        # print "num of words: " + str(numOfWords)
	        realSentiment = 0
	        
	        # determine the real value of sentiment in tweet
	        if numOfWords:
	            realSentiment = float(mySum/math.sqrt(numOfWords))
	        if(realSentiment):
	            myStr = "krypto: " + krypto_ID + "tweet: " + tweet_text + ", sentiment: " + str(realSentiment)
	            print myStr
	            print
	            sentiment_tuple= (krypto_ID, tweet_id, realSentiment)
	            # put sentiment of tweet along with tweet id and identificator if kryptocurrency to buffer
	            if not sentimentBuffer.full():
	                sentimentBuffer.put(sentiment_tuple)
        except:
            e = sys.exc_info()[0]
            print("main thread Error: %s" % e)



def main():
	# setting up spark context
    conf = SparkConf()
    conf.setMaster('local')
    conf.setAppName("TwitterApp")
    # create spark instance with the above configuration
    sc = SparkContext(conf=conf)
    sc.setLogLevel("ERROR")

    # dictionary with words and their sentiment
    filenameAFINN = "./AFINN/AFINN-111.txt"
    afinn = dict(map(lambda (w, s): (w, int(s)), [ ws.strip().split('\t') for ws in open(filenameAFINN) ]))

    tweetBuffer = Queue.Queue(200)
    sentimentBuffer = Queue.Queue(200)

    # getting array of coins which should be analysed
    query_url = 'http://localhost:9401/coin'

    response = requests.get(query_url)
    print response.content
    json_data = json.loads(response.content)
    tmp = json_data["_embedded"]
    coins_json = tmp['coin']
    kryptocurrencies =[]
    for c in coins_json:
        commonCode = str(c['commonCode'])
        hashtags = ",".join(c['hashtags'])
        kryptocurrencies.append((commonCode,hashtags))
    # kryptocurrencies = [('BTC', 'bitcoin,#bitcoin,@bitcoin,Bitcoin,#Bitcoin,@Bitcoin')]#,('LTC', 'litecoin,#litecoin,@litecoin,Litecoin,#Litecoin,@Litecoin')]
    kryptothreads = []
    for krpt in kryptocurrencies:
        myThread = tweetDownloadThread(krpt, tweetBuffer)
        myThread.start()
        kryptothreads.append(myThread)

    print "after threads start"

    postThread = postRestThread(sentimentBuffer)
    postThread.start()

    sparkAnalysisOfSentiment(tweetBuffer, sentimentBuffer, sc, afinn)


main()






