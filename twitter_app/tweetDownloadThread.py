import threading
import requests_oauthlib, requests
import json


# class for downloading tweets from twitter
class tweetDownloadThread(threading.Thread):
    listOfSearchedWords = None  # type: str
    # athourizaton informations for log into twitter
    ACCESS_TOKEN = '979027652823650304-mK2iM4VjlvACoYcVaRBgCCKR1Rp9KxX'
    ACCESS_SECRET = 'Kefbg6XbIfD6QlFcgaoPPKmzmxu5sFEEYyu8mFs0DGnxq'
    CONSUMER_KEY = 'R42Il5Kp36nbLGX1ZYX0i5Vwx'
    CONSUMER_SECRET = 'LhNGYahrVPZbTM3CuDd0WWX7BNeyZ4YHaskDlQJ8d9qx2ztJkD'

    def __init__(self, listOfSearchedWords, outputBuffer):
        threading.Thread.__init__(self)
        self.listOfSearchedWords = listOfSearchedWords
        self.outputBuffer = outputBuffer
        self.my_auth = requests_oauthlib.OAuth1(self.CONSUMER_KEY, self.CONSUMER_SECRET, self.ACCESS_TOKEN, self.ACCESS_SECRET)

    # setting url for downlading data
    # returns url response
    def get_tweets(self):
        url = 'https://stream.twitter.com/1.1/statuses/filter.json'
        query_data = [('language', 'en'), ('track', self.listOfSearchedWords[1])] 
        query_url = url + '?' + '&'.join([str(t[0]) + '=' + str(t[1]) for t in query_data])
        response = requests.get(query_url, auth=self.my_auth, stream=True)
        print(query_url, response)
        return response

    # iteration through response lines and sending them to buffer
    def run(self):
        http_response = self.get_tweets()
        for line in http_response.iter_lines():
            try:
                full_tweet = json.loads(line)
                tweet_tuple = (self.listOfSearchedWords[0],full_tweet)
                if not self.outputBuffer.full():
                    self.outputBuffer.put(tweet_tuple)
            except Exception as e:
               print e.message
