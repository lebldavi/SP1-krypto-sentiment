import threading
import requests_oauthlib, requests
import json
import demjson

# class for sending information about sentiment to crypto_app using POST
class postRestThread(threading.Thread):

    def __init__(self, sentimentBuffer):
        threading.Thread.__init__(self)
        self.sentimentBuffer = sentimentBuffer
        self.url = 'http://localhost:9401/sentiment-data/'

    # taking sentiment from buffer and sending it using POST
    def run(self):
        while True:
            try:
                sentimentTweet = None
                if not self.sentimentBuffer.full():
                    sentimentTweet = self.sentimentBuffer.get()
                if sentimentTweet is None:
                    continue

                query_url = self.url + sentimentTweet[0]
                data = [{'sentimentValue':sentimentTweet[2],'tweetMessage':sentimentTweet[1]}]
                json_data = demjson.encode(data)
                response = requests.post(query_url,json={
                    'sentimentValue': sentimentTweet[2],
                    'tweetMessage': sentimentTweet[1]
                })
                print str(sentimentTweet) + " response: " + str(response)

            except Exception as e:
               print e.message
