# Sentiment 2.0 (SP2) -  whitepaper
V SP1 jsme si zkoušeli vytvořit jednoduchý sentiment ze zdroje Twittru, který ohodnocoval tweety na základě zabarvenosti použitých slov. Vyhodnocený sentiment jsme zobrazovali v grafu a snažili se najít provázanost s ostatními tweety. Tato část se nám příliš nepovedla, jelikož jsme pořádně nevěděli co chceme a neměli jsem žádne skušenosti s data miningem.

Tentokrát v SP2 zkusíme trochu jiný přistup, kde vezmeme více druhů a zdrojů sentimentu a pomocí data-miningu najdeme vhodný model, který bude predikovat pohyby trhu a ukazovat jeho hlavní manipuláotry.

## Návrh na vyhodnocování sentimentu
Sentiment budeme vyhodnocovat z vícero zdrojů a druhů skupin (lidí):
 * veřejnost
 * traidery - vybraná menší skupina
 * media - ovlivnujíci veřejnost
 * SHORT/LONG (open interest, funding) - **úskutečněné** pozice na burze
 * (případně další technické indikátroy)

 **Zdroje sentimentu:**
  * twitter FUD - veřejnost
  * Google Trends - veřejnost
  * twitter (selected) traders TA - short/long příkazy
  * BitMEX & Bitfinex open interest (funding rate)
  * média TA (CNBC Fast Money)
  * další: (BitMEX liqudation & slippage, large block transaction move, Tether move, ...

**FUD (fear, uncertainty, doubt):**

Původní sentiment z SP1 nám umožnuje primitivním způsobem ohodnotit tweety pro různé kryptoměny ($btc, #btc, $eth, ...). Kromě ohodnocování zabarvenosti věty, rozšíříme tuto část o vlastní slovník / fráze / crypto-slang, které pravděpodobně budou daleko přesnějš odpovídat veřejnému sentimentu a náznakům FUD.

Navíc ohodnocení tweetu rozdělíme aspon do 3 kategorii, na které případně použijeme lineární regresi při data-miningu:

| negative | neutral | positive |
| :------------- | :------------- | :------------- |
| berish | will wait | bullish |
| sell all | hodl | moon |

**TA od traiderů:**

Dalším rozšířením budou tweety od vybraných traiderů. Zde budeme opět vyhodnocovat sentiment pouze vybraných frází, ale hlavním indikátorem bude TA(technická analýza PA), tedy SHOR/LONG příkazy.

Kategorie ohodnocení by mohly vypadat třeba takto:

| ... | negative-pattern | neutral | positive-pattern | opportunity | long-short | longshort-term |
| :------------- | :------------- | :------------- | :------------- | :------------- | :------------- | :------------- |
| ... | revisit | chop | bullish SFP | waiting for bounce | SHORT | HTF |
| ... | very low volume | would NOT short/long | strong support | market-FOMO | LONG | LTF 15min |



**Google Trends:**

Taky zajimavý sentiment tvoří vyhledávání různých frází na googlu, např: "is bitcoin dead", "altcoin season", "will bitcoin go back up", "next coin on coinbase". Zde díky google trands dostaneme četnost vyhledávání vybraných frází v čase, tudíš jednoduše můžeme naučit náš model na historických datech.

![google_trands_and_price](uploads/bitcoin_trend.png)

**Realizovaný sentiment - Whales subscription:**

Jako doplňkové data můžeme brát data přímo z burzy, tedy především otevřené pozice SHORT/LONG. Z těchto dat jde pozorovat realizovaný sentiment, který využivají velcí hráči například v podobě short squeezu.

Tento druh sentimentu zahrnuje zminovaný long/short pozice, slippage & size - hidden buy wall, a velké blockchain transakce směrující na burzy, které můžou vzbuzovat FUD. To také souvisý s Funding Rate na BitMEXu u nekonečných kontraktů.

![slippage](uploads/whale_slippage.png)


# Cíle a milestony:
Vzhledem k tomu že je toho více než pravděpodobně budem moc zvlánout, stojí za otázku jakým způsobem sentiment analyzovat a na co se spíše zaměřit. 

Dalo by se zaměřit na přesnejší analýzu tweetů(veřejnost) s real-time aplikací, ale osobně bych asi dal přednost použít více zdrojů a sentiment analyzovat jen v historii. 
(Pozdejší rozšíření (třeba v BP) takovéto verze a převedení na real-time by nemuselo být tolik náročné a mohlo by dát lepší přehled na co se zaměřit nejvíce)

TODO - prokonzultovat

Potencionální úkoly - podle priorit:
 * stáhnout tweety pro veřejnost, uložit zatím do csv + data-mining
 * načítat Google Trands + data-mining
 * stáhnout tweety od traiderů
 * data-mining všeho dohromady (tweety veřejnost + google trands + tweety trajdeři)
 * data z BitMEXu
 * média
 * real-time úkládání a vyhodnocování sentimentu pro víše zmíněné
 * slippage, blockchain transakce, ...


## Zaměření členů v týmu:
TODO (fill out)
 * David Lebl: data-mining, (big-data), archytektura appky, a další.
 * Patrik Kubec: ...
 * Filip Geletka: Google Trends,...
 * Maxim Sachok: ...


## Technologie:
 * Java - SpringBoot, Vaadin 10: systém pro konfiguraci a zapouzdření microservices jako webovou aplikace
 * Python, Jupyter Notebook: data-mining
 * Big Data - Spark, Cassandra?

#

 **PS: jsem to trochu přehnal... :D **
